//
//  Photo.swift
//  iOSPetApp
//
//  Created by IOS Developer on 11/8/15.
//  Copyright © 2015 PetApp. All rights reserved.
//

import Foundation

class Photo {
    
    var photoId: Int = 0
    var petId: Int = 0
    var isAvatar: Bool = false
    var comment: String = ""
    var picture: String = ""
}
