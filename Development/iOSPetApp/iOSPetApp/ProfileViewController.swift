//
//  ProfileViewController.swift
//  iOSPetApp
//
//  Created by IOS Developer on 11/12/15.
//  Copyright © 2015 PetApp. All rights reserved.
//

import UIKit

class ProfileViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var lblFullName: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var imgProfile: UIImageView!

    var fullProfile: FullProfile? = nil
    var petToSend: Pet? = nil
    var tableHeader = "My Pets"
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setViewData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func setImageViewBorder() {
        imgProfile.layer.borderWidth = 0.0
        imgProfile.layer.masksToBounds = false
        imgProfile.layer.cornerRadius = imgProfile.frame.size.width / 2
        imgProfile.clipsToBounds = true
    }
    
    func setViewData() {
        setImageViewBorder()
        lblFullName.text = fullProfile!.firstName + " " + fullProfile!.lastName
        lblEmail.text = fullProfile!.email
        imgProfile.image = self.fullProfile!.profileImage
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        let numbersOfDogs = self.fullProfile?.pets.count
        if  numbersOfDogs == 0 {
            tableHeader = "You don't ave any pet!!!"
        }
        return tableHeader
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let numbersOfDogs = self.fullProfile?.pets.count
        if numbersOfDogs < 3 {
            return numbersOfDogs!
        }
        return 3
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: UITableViewCellStyle.Subtitle, reuseIdentifier: "cell")
        cell.accessoryType = UITableViewCellAccessoryType.DisclosureIndicator
        let entry = self.fullProfile?.pets[indexPath.row]
        
        cell.imageView!.image = entry!.photos[0]
        cell.detailTextLabel?.text = entry?.breed
        cell.textLabel?.text = entry!.name
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        self.petToSend = self.fullProfile?.pets[indexPath.row]
        self.performSegueWithIdentifier("ownPetDetail", sender: self)
    }
    
    override func shouldPerformSegueWithIdentifier(identifier: String, sender: AnyObject?) -> Bool {
        if identifier == "ownPetDetail" {
            if self.petToSend == nil {
                return false
            }
        }
        return true
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "ownPetDetail" {
            let petDetailVC: PetDetailViewController = segue.destinationViewController as! PetDetailViewController
            petDetailVC.pet = self.petToSend
        }
        if segue.identifier == "allMyPets" {
            let allVC: AllPetsViewController = segue.destinationViewController as! AllPetsViewController
            allVC.pets = self.fullProfile!.pets
        }
    }
}
