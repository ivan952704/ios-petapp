//
//  TestData.swift
//  iOSPetApp
//
//  Created by IOS Developer on 11/14/15.
//  Copyright © 2015 PetApp. All rights reserved.
//

import Foundation

class TestData {
    class Entry {
        let fileName: String
        let header: String
        let lat: Double
        let lon: Double
        init(fileName: String, header: String, lat: Double, lon: Double) {
            self.fileName = fileName
            self.header = header
            self.lat = lat
            self.lon = lon
        }
    }
    
    //la paz
    //coordinate: CLLocationCoordinate2D(latitude: -12.0877637, longitude: -77.0905489)))
    //sañaverry
    //coordinate: CLLocationCoordinate2D(latitude: -12.095269, longitude: -77.0565276)))
    //el trigal
    //coordinate: CLLocationCoordinate2D(latitude: -12.1273883, longitude: -76.9891888)))
    //surquillo
    //coordinate: CLLocationCoordinate2D(latitude: -12.1180117, longitude: -77.010352)))
    
    let dogs = [
        Entry(fileName: "01.jpg", header: "Foster", lat: -12.0877637, lon: -77.0905489),
        Entry(fileName: "02.jpg", header: "King", lat: -12.095269, lon: -77.0565276),
        Entry(fileName: "03.jpeg", header: "Many", lat: -12.1273883, lon: -76.9891888),
        Entry(fileName: "04.jpg", header: "Fido", lat: -12.1180117, lon: -77.010352),
        Entry(fileName: "05.jpg", header: "Logan", lat: -12.0702557, lon: -76.9601088)
    ]
}
