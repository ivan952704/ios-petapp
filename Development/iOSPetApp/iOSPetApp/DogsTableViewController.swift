//
//  DogsTableViewController.swift
//  iOSPetApp
//
//  Created by IOS Developer on 11/14/15.
//  Copyright © 2015 PetApp. All rights reserved.
//

import UIKit

class DogsTableViewController: UITableViewController {
    
    let data = TestData()
    
    var envio: TestData.Entry? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.separatorStyle = UITableViewCellSeparatorStyle.None
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.data.dogs.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("CustomCell", forIndexPath: indexPath) as! DogsTableViewCell
        let entry = data.dogs[indexPath.row]
        let image = UIImage(named: entry.fileName)
        cell.lblName.text = entry.header
        cell.imgDog.image = image
        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        print("Row: \(indexPath.row)")
        
        self.envio = data.dogs[indexPath.row]
        self.performSegueWithIdentifier("mapIdentifier", sender: self)
    }
    
    override func shouldPerformSegueWithIdentifier(identifier: String, sender: AnyObject?) -> Bool {
        if identifier == "mapIdentifier" {
            if self.envio == nil {
                return false
            }
        }
        return true
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if(segue.identifier == "mapIdentifier") {
            let mapVC: MapViewController = segue.destinationViewController as! MapViewController
            mapVC.pet = self.envio
            self.envio = nil
        }
    }
}
