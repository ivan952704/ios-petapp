//
//  AllPetsViewController.swift
//  iOSPetApp
//
//  Created by IOS Developer on 11/12/15.
//  Copyright © 2015 PetApp. All rights reserved.
//

import UIKit

class AllPetsViewController: UIViewController, UITableViewDataSource {
    
    var tableHeader = "My Pets"
    let numbersOfRows = 7
    
    var pets = [Pet]()
    var petToSend: Pet? = nil
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        let numbersOfDogs = self.pets.count
        if  numbersOfDogs == 0 {
            tableHeader = "You don't ave any pet!!!"
        }
        return tableHeader
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.pets.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: UITableViewCellStyle.Subtitle, reuseIdentifier: "cell")
        cell.accessoryType = UITableViewCellAccessoryType.DisclosureIndicator
        let entry = self.pets[indexPath.row]
        
        cell.imageView!.image = entry.photos[0]
        cell.detailTextLabel?.text = entry.breed
        cell.textLabel?.text = entry.name
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        self.petToSend = self.pets[indexPath.row]
        self.performSegueWithIdentifier("ownAllPetDetail", sender: self)
    }
    
    override func shouldPerformSegueWithIdentifier(identifier: String, sender: AnyObject?) -> Bool {
        if identifier == "ownAllPetDetail" {
            if self.petToSend == nil {
                return false
            }
        }
        return true
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "ownAllPetDetail" {
            let petDetailVC: PetDetailViewController = segue.destinationViewController as! PetDetailViewController
            petDetailVC.pet = self.petToSend
        }
    }

}
