//
//  ViewController.swift
//  iOSPetApp
//
//  Created by Proyecto on 11/5/15.
//  Copyright © 2015 PetApp. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var btnLogin: UIButton!
    @IBOutlet weak var txtUsername: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    
    var profileToSend: FullProfile? = nil
    var fullProfileParser = FullProfileParser()
    var fullProfileParser2 = FullProfileParser()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setButtonBorder()
        self.fullProfileParser.getProfile(1)
        self.fullProfileParser2.getProfile(2)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.view.endEditing(true)
    }

    func setButtonBorder() {
        btnLogin.layer.cornerRadius = 5
        btnLogin.layer.borderWidth = 2
        btnLogin.layer.borderColor = UIColor.blackColor().CGColor
    }
    
    func showAlert() {
        let alert = UIAlertController(title: "PetApp Login", message: "Username or Password is incorrect", preferredStyle: UIAlertControllerStyle.Alert)
        let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil)
        alert.addAction(okAction)
        presentViewController(alert, animated: true, completion: nil)
    }
    
    @IBAction func btnLoginTouchUpInside(sender: UIButton) {
        txtUsername.resignFirstResponder()
        txtPassword.resignFirstResponder()
        
        if txtUsername.text == "root" && txtPassword.text == "root" {
            self.profileToSend = self.fullProfileParser.profile
        }
        else
        if txtUsername.text == "root2" && txtPassword.text == "root" {
            self.profileToSend = self.fullProfileParser2.profile
        }
        else {
            showAlert()
        }
    }
    
    override func shouldPerformSegueWithIdentifier(identifier: String, sender: AnyObject?) -> Bool {
        if identifier == "userLoginSegue" {
            if self.fullProfileParser.profile == nil || self.fullProfileParser2.profile == nil {
                    return false
                }
            }
        return true
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "userLoginSegue" {
            let profileVC: ProfileViewController = segue.destinationViewController as! ProfileViewController
            profileVC.fullProfile = self.profileToSend
        }
    }
}

