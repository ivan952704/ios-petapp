//
//  User.swift
//  iOSPetApp
//
//  Created by IOS Developer on 11/8/15.
//  Copyright © 2015 PetApp. All rights reserved.
//

import Foundation

class User {
    
    var userId: Int = 0
    var email: String = ""
    var password: String = ""
}
