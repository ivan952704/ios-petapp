//
//  FullProfileParser.swift
//  iOSPetApp
//
//  Created by Proyecto on 11/19/15.
//  Copyright © 2015 PetApp. All rights reserved.
//

import UIKit
import Foundation
import Alamofire
import SwiftyJSON

class FullProfileParser {
    var profile: FullProfile? = nil
    
    func getProfile (profileId : Int) {
        let baseUrl = "https://pet-app-3-ivan952704.c9users.io"
        Alamofire.request(.GET, baseUrl + "/profiles/" + String(profileId) + ".json")
            .responseJSON {
                response in
                if let JSON = response.result.value {
                    let result = SwiftyJSON.JSON(response.result.value!)
                    
                    let currentProfile = result["profile"]
                    self.profile = FullProfile()
                    self.profile!.userId = String(currentProfile["user_id"])
                    self.profile!.profileId = String(currentProfile["id"])
                    self.profile!.firstName = String(currentProfile["first_name"])
                    self.profile!.lastName = String(currentProfile["last_name"])
                    self.profile!.email = String(currentProfile["email"])
                    
                    let profileUrl = baseUrl + String(currentProfile["avatar"])
                    self.profile!.profilePictureUrl = profileUrl
                    let img: UIImage? = self.getImageView(profileUrl)
                    if img != nil {
                        self.profile!.profileImage = img
                    }

                    let jsonPets = result["pets"]
                    if jsonPets.count != 0 {
                        for i in 0...jsonPets.count - 1 {
                            let currentPet = jsonPets[i]
                            let pet = Pet()
                            pet.petId = String(currentPet["id"])
                            pet.userId = String(currentPet["user_id"])
                            pet.name = String(currentPet["name"])
                            pet.pedigree = Bool(currentPet["pedigree"])
                            pet.gender = String(currentPet["gender"])
                            pet.breed = String(currentPet["breed"])
                            pet.bornAt = String(currentPet["born_at"])
                            pet.about = String(currentPet["about"])
                            
                            let jsonPhotos = currentPet["photos"]
                            for i in 0...jsonPhotos.count - 1 {
                                let url = baseUrl + String(jsonPhotos[i]["picture_url"])
                                pet.photosUrl.append(url)
                                
                                let img: UIImage? = self.getImageView(url)
                                if img != nil {
                                    pet.photos.append(img!)
                                }
                            }
                            
                            self.profile?.pets.append(pet)
                        }
                    }
                }
        }
    }
    
    func getImageView(url: String) -> UIImage? {
        let urlToParse = NSURL(string: url)
        let content = NSData(contentsOfURL: urlToParse!)
        if content != nil {
            return UIImage(data: content!)
        }
        return nil
    }
}

//http://stackoverflow.com/questions/30197985/swift-alamofire-swiftyjson-asynchronous-synchronous-class-methods
//http://www.raywenderlich.com/78568/create-slide-out-navigation-panel-swift
