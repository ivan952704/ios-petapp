//
//  DogsTableViewCell.swift
//  iOSPetApp
//
//  Created by IOS Developer on 11/14/15.
//  Copyright © 2015 PetApp. All rights reserved.
//

import UIKit

class DogsTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var imgDog: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}