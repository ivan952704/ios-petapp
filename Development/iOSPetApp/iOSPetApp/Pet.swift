//
//  Pet.swift
//  iOSPetApp
//
//  Created by IOS Developer on 11/8/15.
//  Copyright © 2015 PetApp. All rights reserved.
//

import Foundation
import UIKit

class Pet {
    
    var petId: String = ""
    var userId: String = ""
    var name: String = ""
    var breed: String = ""
    var pedigree: Bool = false
    var bornAt: String = ""
    var gender: String = ""
    var about: String = ""
    
    var photosUrl = [String]()
    
    var photos = [UIImage]()
}
