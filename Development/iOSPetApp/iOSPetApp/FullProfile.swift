//
//  FullProfile.swift
//  iOSPetApp
//
//  Created by Proyecto on 11/19/15.
//  Copyright © 2015 PetApp. All rights reserved.
//

import Foundation
import UIKit

class FullProfile {
    
    var profileId: String = ""
    var userId: String = ""
    var firstName: String = ""
    var lastName: String = ""
    var email: String = ""
    var lat: Double = 0
    var long: Double = 0
    var profilePictureUrl: String = ""
    var profileImage: UIImage? = nil
    
    
    var pets = [Pet]()
}
