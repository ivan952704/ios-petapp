//
//  RegisterViewController.swift
//  iOSPetApp
//
//  Created by IOS Developer on 11/11/15.
//  Copyright © 2015 PetApp. All rights reserved.
//

import UIKit

class RegisterViewController: UIViewController {
    
    @IBOutlet weak var btnCreate: UIButton!
    
    @IBOutlet weak var txtUsername: UITextField!
    
    @IBOutlet weak var txtPassword: UITextField!
    
    @IBOutlet weak var txtFirstName: UITextField!
    
    @IBOutlet weak var txtLastName: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func setButtonBorder() {
        btnCreate.layer.cornerRadius = 5
        btnCreate.layer.borderWidth = 2
        btnCreate.layer.borderColor = UIColor.blackColor().CGColor
    }
    
    @IBAction func btnCreateTouchUpInside(sender: UIButton) {
        txtUsername.resignFirstResponder()
        txtPassword.resignFirstResponder()
        txtFirstName.resignFirstResponder()
        txtLastName.resignFirstResponder()
    }
}