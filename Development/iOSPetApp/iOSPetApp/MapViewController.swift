
import UIKit
import MapKit

class MapViewController: UIViewController {
    
    @IBOutlet weak var mapView: MKMapView!
    var pet: TestData.Entry? = nil
    var artworks: [Artwork] = []
    let regionRadius = 1000
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let initialLocation = CLLocation(latitude: pet!.lat, longitude: pet!.lon)
        centerMapOnLocation(initialLocation)
        
        mapView.delegate = self
        artworks.append( Artwork(title: pet!.header,
            locationName: "This is My House!!!",
            discipline: "Edication",
            coordinate: CLLocationCoordinate2D(latitude: pet!.lat, longitude: pet!.lon)))
        
        mapView.addAnnotations(artworks)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func centerMapOnLocation(location: CLLocation){
        let coordinateRegion = MKCoordinateRegionMakeWithDistance(location.coordinate, 2000, 2000)
        mapView.setRegion(coordinateRegion, animated: true)
    }
}
