
import UIKit

class PetDetailViewController: UIViewController {
    
    @IBOutlet weak var imgPet: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblBreed: UILabel!
    @IBOutlet weak var lblGender: UILabel!
    @IBOutlet weak var lblAbout: UILabel!
    
    var name: String = ""
    var pet: Pet? = nil
    var imageIndex: Int = 0
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setViewData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func replaceImgPet(index: Int) {
        self.imgPet.image = pet?.photos[index]
    }
    
    func setViewData() {
        self.lblName.text = "Name: " + self.pet!.name
        self.lblBreed.text = "Breed: " + self.pet!.breed
        self.lblGender.text = "Gender: " + self.pet!.gender
        self.lblAbout.text = "About me: " + self.pet!.about
        replaceImgPet(imageIndex)
    }
    
    @IBAction func btnNextTouchUpInside(sender: UIButton) {
        imageIndex++
        if self.pet?.photos.count == imageIndex{
            imageIndex = 0
        }
        replaceImgPet(imageIndex)
    }
}
